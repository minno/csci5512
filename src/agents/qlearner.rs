extern crate rand;

use self::rand::Rng;
use guess::{Answer, Info, self};
use agents::Agent;
use array2d::Array2d;

use std::f32;

const DISCOUNT_RATE: f32 = 1.0;
const LEARN_RATE: f32 = 0.01;

pub struct QLearner {
    settings: guess::Settings,
    //pub q_values: [[f32; ACTION_INDEX_MAX]; INFO_INDEX_MAX],
    pub q_values: Array2d<f32>,
}

impl QLearner {
    pub fn new(settings: guess::Settings) -> QLearner {
        let action_index_max = (settings.num_pegs as usize +1)*(settings.num_pegs as usize);
        let info_index_max = (settings.num_pegs as usize +1)*(settings.num_pegs as usize +1);
        QLearner {
            settings: settings,
            q_values: Array2d::new((action_index_max, info_index_max), 0.0),
        }
    }
    
    fn choose_action(&self, state: Info, rand_rate: f32) -> Action {
        let mut rng = rand::thread_rng();
        let action_idx = if rng.gen::<f32>() < rand_rate {
            rng.gen_range(1, self.settings.num_pegs) as usize
        } else {
            self.best_action(state.to_index(self.settings))
        };
        Action::from_index(action_idx, self.settings)
    }
    
    fn best_action(&self, state: usize) -> usize {
        let ref action_qs = self.q_values[state];
        action_qs.iter().cloned().enumerate().skip(1) // To skip the "do nothing" action, which never helps
            .fold((1, f32::NEG_INFINITY), |max, q| if q.1 >= max.1 { q } else { max })
            .0
    }
    
    fn step(&self, answer: &Answer, state: &mut Answer, guesses: &mut Vec<Answer>, rand_rate: f32)
        -> (Info, Action, Info) {
        let curr_info = guess::check_guess(&state, answer);
        let action = self.choose_action(curr_info, rand_rate);
        //println!("Action {:?}", action);
        let mut new_state = state.clone();
        action.apply(&mut new_state);
        //println!("New guess: {:?}", new_state);
        // Make sure that we don't re-try any guess
        /*while guesses.iter().any(|guess| *guess == new_state) {
            new_state = best_so_far.clone();
            action.apply(&mut new_state);
            println!("New guess: {:?}", new_state);
        }*/
        let new_info = guess::check_guess(&new_state, answer);
        guesses.push(new_state.clone());
        if self.reward(new_info) >= self.reward(curr_info) {
            *state = new_state;
            (curr_info, action, new_info)
        } else {
            (curr_info, action, curr_info)
        }
    }
    
    /// Gives the reward function for a given state.
    fn reward(&self, state: Info) -> f32 {
        let max_reward: f32 = BLACK_WEIGHT * (self.settings.num_pegs as f32);
        BLACK_WEIGHT*(state.num_black as f32) + WHITE_WEIGHT*(state.num_white as f32) - max_reward
    }
        
    fn learn(&mut self, pre: Info, act: Action, post: Info, learn_rate: f32) {
        let pre_idx = pre.to_index(self.settings);
        let post_idx = post.to_index(self.settings);
        let act_idx = act.to_index(self.settings);
        
        let new_q = self.reward(pre) as f32 + 
            DISCOUNT_RATE*self.q_values[post_idx][self.best_action(post_idx)];
        self.q_values[pre_idx][act_idx] = (1.0 - learn_rate) * self.q_values[pre_idx][act_idx] +
            learn_rate * new_q;
    }
}

impl Agent for QLearner {
    fn solve(&mut self, answer: &Answer) -> u32 {
        let mut curr = Answer::random(self.settings);
        let mut guesses = Vec::new();
        loop {
            let (pre, act, post) = self.step(answer, &mut curr, &mut guesses, LEARN_RATE);
            self.learn(pre, act, post, LEARN_RATE);
            if curr == *answer {
                return guesses.len() as u32;
            }
        }
    }
    
    fn test(&self, answer: &Answer) -> u32 {
        let mut curr = Answer::random(self.settings);
        let mut guesses = Vec::new();
        loop {
            self.step(answer, &mut curr, &mut guesses, 0.0);
            if curr == *answer {
                return guesses.len() as u32;
            }
        }
    }
}

const BLACK_WEIGHT: f32 = 1.05;
const WHITE_WEIGHT: f32 = 1.0;

#[derive(Debug, Copy, Clone)]
pub struct Action {
    pub swaps: u32, // At most num_pegs - 1 swaps
    pub muts: u32,  // At most num_pegs mutations
}

impl Action {
    pub fn apply(&self, answer: &mut Answer) {
        for _ in (0..self.swaps) {
            let i1 = rand::thread_rng().gen_range(0, answer.settings.num_pegs as usize);
            let i2 = rand::thread_rng().gen_range(0, answer.settings.num_pegs as usize);
            answer.pegs.swap(i1, i2);
        }
        for _ in (0..self.muts) {
            let i = rand::thread_rng().gen_range(0, answer.settings.num_pegs as usize);
            let new_color = rand::thread_rng().gen_range(0, answer.settings.num_colors);
            answer.pegs[i] = new_color;
        }
    }

    /// Converts the struct into an index suitable for a lookup table.
    pub fn to_index(&self, settings: guess::Settings) -> usize {
        let base = self.swaps as usize * (settings.num_pegs + 1) as usize;
        base + self.muts as usize
    }
    
    // Performs the inverse transformation of to_index
    pub fn from_index(index: usize, settings: guess::Settings) -> Action {
        let num_pegs = settings.num_pegs as usize;
        let swaps = index / (num_pegs + 1);
        let muts = index % (num_pegs + 1);
        Action{ swaps: swaps as u32, muts: muts as u32 }
    }
    
    /// The upper bound on the lookup table index returned by to_index
    /// on any Action that has at most settings.num_pegs total white and
    /// black pegs.
    pub fn index_max(settings: guess::Settings) -> usize {
        let num_pegs = settings.num_pegs as usize;
        num_pegs * (num_pegs+1)
    }
}