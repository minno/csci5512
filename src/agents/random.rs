use guess;
use agents::Agent;

pub struct RandomGuesser;

impl Agent for RandomGuesser {    
    fn test(&self, answer: &guess::Answer) -> u32 {
        let mut num_guesses = 0;
        loop {
            num_guesses += 1;
            let guess = guess::Answer::random(answer.settings());
            if guess == *answer {
                return num_guesses;
            }
        }
    }
}