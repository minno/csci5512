Reinforced Learning Mastermind Agent

The program is built using the Rust language, and 
thus needs to have rust installed to run. Rust is a new
language, and we thus used the nightly builds, and as such
to use this package the nightly compiler must be installed.



Installing Rust:

Website: 	 http://www.rust-lang.org/
Install Website: http://www.rust-lang.org/install.html

Install Script: curl -sSf https://static.rust-lang.org/rustup.sh | sh -s -- --channel=nightly

Build Instructions

cargo build

Note: "cargo update" and "cargo clean" may help resolve errors.

Run Instructions:

cargo run -- {options}

Options:

-m <Int> - Max Number of time to iterate each run
-n <Int> - Number of times to perform runs
-c <Int> - Number of Colors
-p <Int> - Number of Colors

-a FLAG  - Runs all agents against parameters
-q FLAG  - Runs q-learner and tests
-r FLAG  - Runs random agents

-d FLAG  - Runs dev code

-h FLAG  - Runs Help  

Examples:

- Q-Learner: cargo run -- -m 100000 -c 6 -p 4 -n 3 -q
- Random   : cargo run -- -m 100000 -c 6 -p 4 -n 3 -r
- All      : cargo run -- -m 100000 -c 6 -p 4 -n 3 -a

