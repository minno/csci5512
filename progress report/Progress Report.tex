\documentclass[12pt]{article}
\usepackage{amsmath}
\usepackage{float}
\usepackage{hyperref}

\title{CSCI 5512: Project Progres Report}
\author{William Fischer, Nabil Cheikh\\
	\text{Reinforcement learning in Mastermind}}
\date{\today}
\begin{document}
\maketitle 

\section*{Problem Statement}

Mastermind is a logic puzzle where the player needs to guess a hidden sequence of colors. For each guess, the player is told how many positions in its guess have the correct color in the correct location, and how many have the correct color in the wrong location. Our goal is to build an agent that can play the game, with the goal of minimizing the expected number of guesses it makes before guessing correctly.

\section*{Background}

There are quite a few effective solutions to this problem. Knuth[2] described an algorithm to solve the variant with 6 colors and 4 entries through a specific guessing strategy. This strategy requires searching a significant part of the solution space to construct, which makes it unfeasible on larger problems. Berghman, Goossens, Leus[3] applied genetic algorithms to the problem, creating an agent that can solve even larger problems quickly, and collected results from other genetic algorithms. Yet another researcher, Kooi[4], described a good heuristic based on a notion of the entropy of the space of remaining possibilities.

\section*{Possible Solution}
We think that this puzzle is a good fit for reinforcement learning. It consists of an unknown state where the agent can take actions to learn information about that state. The agent must be able to learn a strategy between games for information collection, as well as a strategy within the game for using that information to determine the state. The second part, using the results of guesses to determine the solution, has been proven to be NP-hard[1], so a learning-based approach should be much more effective than a reasoning-based approach.

\section*{Plan/Schedule}

\begin{itemize}
  \item \textbf{Complete} Read existing materials on mastermind.  
  \item \textbf{Complete} Analyze Genetic Solution to help build our learning agent.
  \begin{itemize}
  	\item[•] We were able to develop a set of actions based on the genetic algorithms population creation actions as well as the fitness value.
  \end{itemize}
  
  \item \textbf{In Progress (Target 4/24)} Acquire source code to several existing algorithms from the literature to benchmark against our agent. If source code cannot be acquired, we will code any desired agents that are well defined from the papers and will be beneficial for comparison.  This will be done for agents whose source was not readily available.

  \begin{itemize}
  	\item[•] We have reached out to various math professors to get more direct access to one of the papers that has an agent we would like to benchmark against. We are currently exploring other avenues to acquire or write the algorithm used in a Random Sampler and Random Iterator methods.  We are also exploring implementing the Genetic Algorithm[4], as it is very well defined in both pseudo code as well as probabilities and other parameters used. 
  \end{itemize}  
  
  \item \textbf{Complete*} Develop potential experiment parameters, in this case the desired number of colors and pins for each run.
    \begin{itemize}
  	\item[•] We are looking to use the experiment parameters from the Berghman, Goossens, and Leus genetic algorithm paper.  They use ($P = 4, N = 6$), ($P = 5, N = 8$), ($P = 6, N = 9$), ($P = 8, N = 12$) where P is the number of colors and N is the number of pins.  We intend to use at a minimum the same experimental parameters, as it would allow us to compare to the genetic algorithm without implementing and running it ourselves.  We may explore other experimental parameters if there is time.
  \end{itemize}  
  \item \textbf{Complete} Create the base code for the Mastermind game based on the game algorithm from the literature(specific paper).  This code will perform the guess checking for each game, and will be used by all agents.
  
  \item \textbf{Complete} Code the Random and Random Consistent agents, which will not use any AI concepts, but rather will be used for comparison.
  
  \item \textbf{In Progress (Target 4/24)} Build our reinforced learning agent.
      \begin{itemize}
  	\item[•] We have determined the actions that the agent will be able to take, as well as the type of reinforcement learning that we are going to use.  We have decided to use model based ADP. We considered a model free approach, however as it takes more time to achieve a potential optimal solution, we settled on a model based approach, since the state space we plan to use is not very large.
  \end{itemize}  
  
  \item \textbf{Due 4/21} Write the code to run and aggregate data for statistical analysis. This will be code that runs agents with given settings for statistical analysis. It will execute based on the experimental parameters.
  
    \item \textbf{Begin 4/17} Setup test environment.
          \begin{itemize}
  	\item[•] We are going to use at least two i7 systems with 16GB of RAM that can run 24/7 without any other load.  We also have access to a larger system with 2 CPU's and 24 cores and 98GB of RAM that can also run 24/7 without any other load.  The larger system will most likely only be used if we are seeing a large time requirement for the reinforcement learner, and thus need greater resources to meet experimental requirements.
  \end{itemize}  
  
  \item \textbf{Begin 4/24} Begin running finished agents as soon as they are complete based on the experiment parameters.  In this way we can spend more time debugging our agent and running it.
  
   \begin{itemize}
  	\item[•] As we have specified our base experimental parameters, we will begin running the Random and Random Consistent agents as soon as the aggregation code is complete.  We will run other agents as soon as they are acquired or coded, including our reinforcement learner.
  \end{itemize}  
  
  \item \textbf{~Begin 5/1} Analyze our reinforcement agent based on time to learn and solve rate efficiency.
  \item  \textbf{Begin 4/18} Write final report.
  
     \begin{itemize}
  	\item[•] We are going to start laying out the structure of the report and filling in the sections related to background and methodology.  We will then begin filling in the reports results/data section as we run agents.  Thus we will then be able to revise these sections and complete an analyses/results,conclusion and future exploration section.
  \end{itemize} 
\end{itemize}
 
 \section*{Deliverables}
 
  \begin{itemize}
  	\item[•] Source Code Modules (Written in Rust):
  	  \begin{itemize}
  	  	\item Mastermind Guess Checker Game Algorithm
  	 	\item Random Agent
  	 	\item Random Consistent Agent
  	 	\item Reinforcement ADP Agent
  	 	\item Module to run and aggregate data from agents.
  	  \end{itemize} 
  	  
  	  \item[•] Raw and Statistical Analysis Data
  	  \item[•] Final Report
  \end{itemize} 

\section*{Significance and Results}
The results of our work will show another approach to developing a strategy and beating Mastermind quickly.  The enumeration approaches do not hold up well in larger versions of the game, that is more colors and more pins.  The genetic algorithm approach is very fast, and does not have the same problem space issues that enumeration has as the problem space increases.  Thus we are seeking to provide another alternative to enumeration solutions in Mastermind.  The results of our work should be able to show whether or not our approach using reinforcement learning is a viable and competitive approach to solving Mastermind. 

\section*{Citations}

\begin{description}



\item[•] [1] - "Mastermind is NP-Complete" - Jeff Stuckman, Guo-Qiang Zhang - \url{arxiv.org/abs/cs/0512049}

\item[•] [2] - "The Computer as Master Mind" - Donald E. Knuth -
\url{http://www.dcc.fc.up.pt/~sssousa/RM09101.pdf}

\item[•] [3] - "Efficient solutions for Mastermind using genetic algorithms" - Lotte Berghman, Dries Goossens, Roel Leus  - \url{https://lirias.kuleuven.be/bitstream/123456789/164803/1/kbi_0806.pdf}

\item[•] [4] - "Yet Another Mastermind Strategy" - Barteld Kooi - \url{http://www.researchgate.net/profile/Barteld_Kooi/publication/30485793_Yet_another_mastermind_strategy/links/0c96051c9a68c6f6bd000000.pdf}

\end{description}

\end{document}

