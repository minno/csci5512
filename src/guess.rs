extern crate rand;
use self::rand::Rng;
use std::cmp;

/// The parameters for playing a game of Guess.
#[derive(Copy, Clone, Debug, PartialEq, Eq)]
pub struct Settings {
    pub num_colors: u32,
    pub num_pegs: u32,
}

impl Settings {
    pub fn new(num_colors: u32, num_pegs: u32) -> Settings {
        Settings{ num_colors: num_colors, num_pegs: num_pegs }
    }
}

/// The solution to a game of Guess.
#[derive(Debug, PartialEq, Eq, Clone)]
pub struct Answer {
    pub pegs: Vec<u32>,
    pub settings: Settings,
}

impl Answer {
    /// Generates a new answer using the given peg values.
    ///
    /// # Panics
    /// Panics if there is an invalid peg value, or an incorrect number
    /// of them.
    pub fn new(pegs: &[u32], settings: Settings) -> Answer {
        assert!(pegs.iter().all(|&num| num < settings.num_colors));
        assert_eq!(pegs.len(), settings.num_pegs as usize);
        Answer{ pegs: Vec::from(pegs), settings: settings }
    }

    /// Generates a new answer using random pegs according to the given
    /// game parameters.
    pub fn random(settings: Settings) -> Answer {
        let mut rng = rand::thread_rng();
        let pegs = (0..settings.num_pegs)
            .map(|_| rng.gen_range(0, settings.num_colors))
            .collect();
        Answer{ pegs: pegs, settings: settings }
    }
    
    /// Returns a copy of the game parameters for this answer.
    pub fn settings(&self) -> Settings {
        self.settings
    }
}

/// The result of a single trial of Guess.
#[derive(Debug, PartialEq, Eq, Copy, Clone)]
pub struct Info {
    pub num_black: u32,
    pub num_white: u32,
}

impl Info {
    /// Converts the struct into an index suitable for a lookup table.
    /// For a given maximum number of pegs, these indices fill up approximately
    /// half of the range 0..index_max.
    pub fn to_index(&self, settings: Settings) -> usize {
        let base = self.num_black as usize * (settings.num_pegs + 1) as usize;
        base + self.num_white as usize
    }
    
    // Performs the inverse transformation of to_index
    pub fn from_index(index: usize, settings: Settings) -> Info {
        let num_pegs = settings.num_pegs as usize;
        let num_black = index / (num_pegs + 1);
        let num_white = index % (num_pegs + 1);
        Info{ num_black: num_black as u32, num_white: num_white as u32 }
    }
    
    /// The upper bound on the lookup table index returned by to_index
    /// on any Info that has at most settings.num_pegs total white and
    /// black pegs.
    pub fn index_max(settings: Settings) -> usize {
        let num_pegs = settings.num_pegs as usize;
        (num_pegs+1) * (num_pegs+1)
    }
}

/// Evaluates a guess against the solution, and gives the total number of
/// black and white pegs describing how close the guess is to correct.
///
/// # Panics
/// Panics if the answer and guess have different game parameters.
pub fn check_guess(answer: &Answer, guess: &Answer) -> Info {
    assert_eq!(answer.settings, guess.settings);
    let num_black = answer.pegs.iter()
        .zip(guess.pegs.iter())
        .filter(|&(&a, &b)| a == b)
        .count();
    let num_correct_color = (0..answer.settings.num_colors)
        .map(|num| {
            let count_eq = |ans: &Answer| {
                ans.pegs.iter()
                    .filter(|&&x| x == num)
                    .count()
            };
            let count_in_answer = count_eq(answer);
            let count_in_guess = count_eq(guess);
            cmp::min(count_in_guess, count_in_answer)
        })
        .sum::<usize>();
    let num_white = num_correct_color - num_black;
    Info{ num_white: num_white as u32, num_black: num_black as u32 }
}


#[cfg(test)]
mod test {
    use super::*;
    use std::iter;
    
    #[test]
    fn test_check_guess() {
        let settings = Settings::new(6, 4);
        let answer = Answer::new(&[1, 2, 2, 5], settings);
        let guess1 = Answer::new(&[0, 3, 4, 0], settings);
        let guess2 = Answer::new(&[1, 1, 2, 2], settings);
        let guess3 = Answer::new(&[2, 1, 5, 2], settings);
        
        assert_eq!(check_guess(&answer, &answer),
            Info{ num_white: 0, num_black: 4});    
        assert_eq!(check_guess(&answer, &guess1),
            Info{ num_white: 0, num_black: 0});
        assert_eq!(check_guess(&answer, &guess2),
            Info{ num_white: 1, num_black: 2});
        assert_eq!(check_guess(&answer, &guess3),
            Info{ num_white: 4, num_black: 0});
    }

    #[test]
    fn check_random_answer_vaild() {
        let settings = Settings::new(6, 4);
        for _ in (0..10000) {
            let answer = Answer::random(settings);
            Answer::new(&answer.pegs, settings);
        }
    }

    #[test]
    #[should_panic(expected = "assertion failed")]
    fn test_mismatched_settings() {
        let settings1 = Settings::new(6, 4);
        let settings2 = Settings::new(7, 6);
        let guess1 = Answer::random(settings1);
        let guess2 = Answer::random(settings2);
        check_guess(&guess1, &guess2);
    }

    #[test]
    #[should_panic(expected = "assertion failed")]
    fn test_bad_peg_value() {
        let settings = Settings::new(2, 2);
        Answer::new(&[1, 2], settings);
    }

    #[test]
    #[should_panic(expected = "assertion failed")]
    fn test_bad_peg_count() {
        let settings = Settings::new(2, 2);
        Answer::new(&[0, 1, 0, 1], settings);
    }
    
    #[test]
    fn test_info_index() {
        for num_pegs in 1..10 {
            let settings = Settings::new(4, num_pegs);
            let mut counts = vec![0; Info::index_max(settings)];
            for (b, w) in (0..num_pegs+1).flat_map(|x| iter::repeat(x)
                                                       .zip(0..num_pegs+1))
                                         .filter(|&(b, w)| b+w <= num_pegs) {
                let info = Info{ num_white: w, num_black: b };
                counts[info.to_index(settings)] += 1;
                
                assert_eq!(info, Info::from_index(info.to_index(settings), settings));
            }
            
            // Check that each index has exactly one valid Info that maps to it.
            assert!(counts.iter().all(|&x| x == 0 || x == 1));
        }
    }
}