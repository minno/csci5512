use guess;
use agents::Agent;

const NUM_PEGS: usize = 4;
const NUM_COLORS: usize = 6;

const ACTION_INDEX_MAX: usize = (NUM_PEGS+1)*(NUM_PEGS);
const INFO_INDEX_MAX: usize = (NUM_PEGS+1)*(NUM_PEGS+1);

const DISCOUNT_RATE: f32 = 1.0;

const NUM_EXPLORE_TRIALS: u32 = 10;

pub struct Learner {
    settings: guess::Settings,

    /// Table of utilities: U[s] is the estimated utility of the current state.
    utilities: [f32; INFO_INDEX_MAX],
    
    /// Table of probabilities: T[s][a][s'] = probability of going from s to s' given action a
    transition_probabilities: [[[f32; INFO_INDEX_MAX]; ACTION_INDEX_MAX]; INFO_INDEX_MAX],
    
    /// Table of exploration counts: N[a][s] = number of times action a taken in state s
    explore_counts: [[u32; INFO_INDEX_MAX]; ACTION_INDEX_MAX],
}

impl Learner {
    pub fn new() -> Learner {
        Learner {
            settings: guess::Settings{ num_pegs: NUM_PEGS as u32, num_colors: NUM_COLORS as u32 },
            utilities: [0.0; INFO_INDEX_MAX],
            transition_probabilities: [[[0.0; INFO_INDEX_MAX]; ACTION_INDEX_MAX]; INFO_INDEX_MAX],
            explore_counts: [[0; INFO_INDEX_MAX]; ACTION_INDEX_MAX],
        }
    }
    
    fn choose_action(&self, state: guess::Info) -> Action {
        let ref action_result_table = self.transition_probabilities[state.to_index(self.settings)];
        // Get action with the highest utility
        let action_values = (0..ACTION_INDEX_MAX).map(|index| {
            let ref result_table = action_result_table[index];
        });
        Action{ swaps: 0, muts: 0 }
    }
    
    /// The exploration function, estimates rewards of taking this action in this state
    fn exploration(utility: f32, explore_counts: u32) -> f32 {
        if explore_counts < NUM_EXPLORE_TRIALS {
            0.0
        } else {
            utility
        }
    }
}

impl Agent for Learner {
    // A list of starting state, action taken, and ending state.
    type SolveResult = Vec<(guess::Info, Action, guess::Info)>;
    
    fn solve(&self, answer: &guess::Answer) -> (u32, Vec<(guess::Info, Action, guess::Info)>) {
        (0, Vec::new())
    }
}

pub struct Action {
    pub swaps: u32, // At most num_pegs - 1 swaps
    pub muts: u32,  // At most num_pegs mutations
}

impl Action {
    /// Converts the struct into an index suitable for a lookup table.
    pub fn to_index(&self, settings: guess::Settings) -> usize {
        let base = self.swaps as usize * (settings.num_pegs + 1) as usize;
        base + self.muts as usize
    }
    
    // Performs the inverse transformation of to_index
    pub fn from_index(index: usize, settings: guess::Settings) -> Action {
        let num_pegs = settings.num_pegs as usize;
        let swaps = index / (num_pegs + 1);
        let muts = index % (num_pegs + 1);
        Action{ swaps: swaps as u32, muts: muts as u32 }
    }
    
    /// The upper bound on the lookup table index returned by to_index
    /// on any Action that has at most settings.num_pegs total white and
    /// black pegs.
    pub fn index_max(settings: guess::Settings) -> usize {
        let num_pegs = settings.num_pegs as usize;
        num_pegs * (num_pegs+1)
    }
}