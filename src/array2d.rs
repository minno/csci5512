use std::ops::{Index, IndexMut};
use std::vec;

/// A simple class for a fixed-size inline "array of arrays"
#[derive(Clone, Debug)]
pub struct Array2d<T> {
    pub dims: (usize, usize),
    pub nums: Vec<T>,
}

impl<T: Clone> Array2d<T> {
    pub fn new(dims: (usize, usize), init: T) -> Array2d<T> {
        Array2d {
            dims: dims,
            nums: vec::from_elem(init, dims.0 * dims.1)
        }
    }
}

impl<T: Clone> Index<usize> for Array2d<T> {
    type Output = [T];
    
    #[inline]
    fn index(&self, index: usize) -> &[T] {
        let slice_size = self.dims.0;
        &self.nums[slice_size*index..slice_size*(index+1)]
    }
}

impl<T: Clone> IndexMut<usize> for Array2d<T> {
    #[inline]
    fn index_mut(&mut self, index: usize) -> &mut [T] {
        let slice_size = self.dims.0;
        &mut self.nums[slice_size*index..slice_size*(index+1)]
    }
}