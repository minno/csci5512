#![feature(core, convert)]

extern crate getopts;
extern crate time;

use getopts::Options;
use std::env;
use std::str::FromStr;
use std::string::String;
use std::thread;
use std::path::Path;

use std::io::prelude::*;
use std::fs::File;

pub mod guess;
pub mod agents;
pub mod array2d;
use self::agents::Agent;

fn print_usage(program: &str, opts: Options) {
    let brief = format!("Usage: {} [options]", program);
    print!("{}", opts.usage(&brief));
}

fn print_progress_w_avg(title: &str, num: f64, denom: f64, delta: f64, avg_count: i32, counter: i32) {
    let ratio = num / denom;
    
    // Only print if there will be a bigger bar than last time.
    if ((num/denom)/delta) as i32 > (((num-1.0)/denom)/delta) as i32 {
        print!("\r{}: {:.*} [", title,2,ratio);

        for _ in (0..((ratio/delta) as i32)) {
                print!("=");
        }

        for _ in (0..(100-(ratio/delta) as i32)) {
            print!(" ");
        }

        print!("] {:.*}", 10,((avg_count as f64))/((counter+1) as f64) as f64);
    }
}

fn run_random(max: i32, c: u32, p: u32) {
    let settings = guess::Settings::new(c, p);
    let mut agent = agents::random::RandomGuesser;
    let mut counter = 0;
    let mut avg_count = 0 as i32;
    let perc_delta = 1 as f64;
    
    let start = time::now();

    for _ in (0..max) {
        let answer = guess::Answer::random(settings);
        let num_steps = agent.solve(&answer) as i32;
        avg_count += num_steps;

        let num = ((counter+1) as f64) * 100.0;
        let denom = max as f64;

        print_progress_w_avg("Random Guesser", num, denom, perc_delta, avg_count, counter);

        counter += 1;
    }
    
    let end = time::now();

    println!("");
    
    println!("Avg time: {:.2} μs", ((end - start).num_microseconds().unwrap() as f64) / (max as f64));
        
    return;
}

fn run_random_consistent(max: i32, c: u32, p: u32) {
    let settings = guess::Settings::new(c, p);
    let mut agent = agents::random_consistent::RandomConsistent;
    let mut counter = 0;
    let mut avg_count = 0 as i32;
    let perc_delta = 1 as f64;
    
    let start = time::now();

    for _ in (0..max) {
        let answer = guess::Answer::random(settings);
        let num_steps = agent.solve(&answer) as i32;
        avg_count += num_steps;

        let num = ((counter+1) as f64) * 100.0;
        let denom = max as f64;

        print_progress_w_avg("Random Consistent", num, denom, perc_delta, avg_count, counter);

        counter += 1;
    }
    
    let end = time::now();

    println!("");
    
    println!("Avg time: {:.2} μs", ((end - start).num_microseconds().unwrap() as f64) / (max as f64));
        
    return;
}

fn run_random_no_repeat(max: i32, c: u32, p: u32) {
    let settings = guess::Settings::new(c, p);
    let mut agent = agents::random_no_repeat::RandomNoRepeat;
    let mut counter = 0;
    let mut avg_count = 0 as i32;
    let perc_delta = 1 as f64;
    
    let start = time::now();

    for _ in (0..max) {
        let answer = guess::Answer::random(settings);
        let num_steps = agent.solve(&answer) as i32;
        avg_count += num_steps;

        let num = ((counter+1) as f64) * 100.0;
        let denom = max as f64;

        print_progress_w_avg("Random No Repeat", num, denom, perc_delta, avg_count, counter);

        counter += 1;
    }
    
    let end = time::now();

    println!("");
    
    println!("Avg time: {:.2} μs", ((end - start).num_microseconds().unwrap() as f64) / (max as f64));
        
    return;
}

fn run_qlearner(max: i32, c: u32, p: u32) {
    let settings = guess::Settings::new(c, p);
    let mut agent = agents::qlearner::QLearner::new(settings);
    let mut counter = 0;
    let mut avg_count = 0 as i32;
    let perc_delta = 1 as f64;

    for _ in (0..max) {
        let answer = guess::Answer::random(settings);
        let num_steps = agent.solve(&answer) as i32;
        avg_count += num_steps;

        let num = ((counter+1) as f64) * 100.0;
        let denom = max as f64;

        print_progress_w_avg("Q Learner - Solve", num, denom, perc_delta, avg_count, counter);

        counter += 1;
    }

    avg_count = 0;
    counter = 0;

    println!("");
    
    let start = time::now();

    for _ in (0..max) {
        let answer = guess::Answer::random(settings);
        let num_steps = agent.test(&answer) as i32;
        avg_count += num_steps;
    
        let num = ((counter+1) as f64) * 100.0;
        let denom = max as f64;
        
        print_progress_w_avg("Q Learner - Test", num, denom, perc_delta, avg_count, counter);

        counter += 1;
    }
    
    let end = time::now();

    println!("");
    
    println!("Avg time: {:.2} μs", ((end - start).num_microseconds().unwrap() as f64) / (max as f64));
        
    return;
}

#[cfg(not(test))]
fn main() {
    //let mut f = try!(File::create("foo.csv").is_ok());
    
    let mut n = 1;
    let mut max = 10000;
    let mut c = 6;
    let mut p = 4;

    let mut count = 1;

    let args: Vec<String> = env::args().collect();
    let program = args[0].clone();

    let mut opts = Options::new();
    opts.optopt("o", "", "set output file name", "NAME");
    opts.optopt("m", "max", "set the number of times to iterate each run", "COUNT");
    opts.optopt("n", "iter", "set the number of times to perform runs", "COUNT");
    opts.optopt("c", "color", "set the number of colors", "COUNT");
    opts.optopt("p", "pegs", "set the number pegs", "COUNT");

    opts.optflag("a", "all", "runs all agents");
    
    opts.optflag("q", "qlearner", "runs  reinforcement learner");
    opts.optflag("r", "random", "runs all random agents");

    opts.optflag("t", "random", "runs all random agents");
    
    opts.optflag("d", "dev", "runs dev code");
    
    opts.optflag("h", "help", "print this help menu");

    let matches = match opts.parse(&args[1..]) {
        Ok(m) => { m }
        Err(f) => { panic!(f.to_string()) }
    };
        
    if matches.opt_present("h") {
        print_usage(&program, opts);
        return;
    }

    if matches.opt_present("n") {

        let n_result = <i32>::from_str(matches.opt_str("n").unwrap().as_str());

        n = match n_result.ok() {
            Some(x) => x,
            None => 0
        };
    }

    if matches.opt_present("m") {

        let m_result = <i32>::from_str(matches.opt_str("m").unwrap().as_str());

        max = match m_result.ok() {
            Some(x) => x,
            None => 0
        };
    }

    if matches.opt_present("c") {

        let c_result = <u32>::from_str(matches.opt_str("c").unwrap().as_str());

        c = match c_result.ok() {
            Some(x) => x,
            None => 0
        };
    }

    if matches.opt_present("p") {

        let p_result = <u32>::from_str(matches.opt_str("p").unwrap().as_str());

        p = match p_result.ok() {
            Some(x) => x,
            None => 0
        };
    }

    println!("Color: {} - Pegs: {}", c,p);


    if matches.opt_present("a") {
        
        for _ in (0..n) {
            println!("\nRun - {}/{}",count,n);

            run_qlearner(max,c,p);
            run_random(max,c,p);
            run_random_consistent(max,c,p);
            run_random_no_repeat(max,c,p);

            
            count += 1;

        }
    }

    if matches.opt_present("q") {
        for _ in (0..n) {
            println!("\nRun - {}/{}",count,n);
            
            run_qlearner(max,c,p);

            count += 1;
        }
    }

    if matches.opt_present("r") {
        for _ in (0..n) {
            println!("\nRun - {}/{}",count,n);
            
            run_random(max,c,p);
            run_random_consistent(max,c,p);
            //run_random_no_repeat(max,c,p);

            count += 1;
        }
    }
    

    /*if matches.opt_present("t") {
        let child_1 = thread::spawn(move || {
            run_random(max,c,p);
        });

        let child_2 = thread::spawn(move || {
            run_random_consistent(max,c,p);
        });

        let child_3 = thread::spawn(move || {
            run_random_no_repeat(max,c,p);
        });

        let _res_1 = child_1.join();
        let _res_2 = child_2.join();
        let _res_3 = child_3.join();
    }*/

    if matches.opt_present("d") {
        let settings = guess::Settings::new(6, 4);
        let mut agent = agents::qlearner::QLearner::new(settings);
        let mut counter = 0;
        for _ in (0..1000000) {
            let answer = guess::Answer::random(settings);
            let num_steps = agent.solve(&answer);
            println!("{},{}", counter, num_steps);
            counter += 1;
        }
        println!("{:?}", agent.q_values);
    }

}
