#![feature(core)]

use std::f64;

pub mod guess;
pub mod agents;
pub mod array2d;
use self::agents::Agent;

#[cfg(not(test))]
fn main() {

    let settings = guess::Settings::new(6, 4);
    let mut agent = agents::qlearner::QLearner::new(settings);
    let mut counter = 0;
    let mut avg_count = 0;
    let max  = 1000000;
    let perc_delta = 1 as f64;

    for _ in (0..max) {
        let answer = guess::Answer::random(settings);
        let num_steps = agent.solve(&answer);
        avg_count += num_steps;
        //println!("{},{},{}", counter, num_steps, (avg_count/(counter+1)));


        // Progress
        let ratio = (((counter+1) as f64)/((max) as f64))* (100 as f64);

        print!("\r{:.*} [", 2,ratio);

        // Show the load bar.
        for _ in (0..((ratio/perc_delta) as i32)) {
            print!("=");
        }

        for _ in (0..(100-(ratio/perc_delta) as i32)) {
            print!(" ");
        }

        print!("] {}", (avg_count/(counter+1)));
        
        counter += 1;
    }

    println!("");

    counter = 0;
    avg_count = 0;

    for _ in (0..max) {
        let answer = guess::Answer::random(settings);
        let num_steps = agent.test(&answer);
        avg_count += num_steps;
        //println!("{},{},{}", counter, num_steps, (avg_count/(counter+1)));


        // Progress
        let ratio = (((counter+1) as f64)/((max) as f64))* (100 as f64);

        print!("\r{:.*} [", 2,ratio);

        // Show the load bar.
        for _ in (0..((ratio/perc_delta) as i32)) {
            print!("=");
        }

        for _ in (0..(100-(ratio/perc_delta) as i32)) {
            print!(" ");
        }

        print!("] {}", (avg_count/(counter+1)));
        
        counter += 1;
    }
}
