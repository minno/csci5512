use guess;
use agents::Agent;
use super::is_consistent;

pub struct RandomConsistent;

// Implements the algorithm described in Rosu (1999)
impl Agent for RandomConsistent {
    fn test(&self, answer: &guess::Answer) -> u32 {
        let mut history: Vec<(guess::Answer, guess::Info)> = Vec::new();
        let mut num_guesses = 0;
        let settings = answer.settings();
        loop {
            num_guesses += 1;
            let mut curr_guess = guess::Answer::random(settings);
            while !is_consistent(&curr_guess, &history) {
                curr_guess = guess::Answer::random(settings);
            }
            let result = guess::check_guess(answer, &curr_guess);
            if result.num_black == answer.settings().num_pegs {
                // Finished!
                return num_guesses;
            }
            history.push((curr_guess, result));
        }
    }
}