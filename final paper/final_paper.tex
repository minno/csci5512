\documentclass[12pt]{article}
\usepackage{amsmath}
\usepackage{amsfonts} 	% for \leadsto
\usepackage{amssymb} 
\usepackage{amsthm}
\usepackage{graphicx}    % needed for including graphics e.g. EPS, PS
\topmargin -1.5cm        
\oddsidemargin -0.04cm
\evensidemargin -0.04cm  % same as oddsidemargin but for left-hand pages
\textwidth 16.59cm
\textheight 21.94cm 
%\pagestyle{empty}       % Uncomment if don't want page numbers
\parskip 7.2pt           % sets spacing between paragraphs
%\renewcommand{\baselinestretch}{1.5} % Uncomment for 1.5 spacing between lines
\parindent 0pt			 % sets leading space for paragraphs

\usepackage{enumitem}% http://ctan.org/pkg/enumitem
\usepackage{caption}			% for \captionof and \captionsetup

\usepackage{dcolumn}
\usepackage{booktabs}
\usepackage{tikz}
\usepackage{array}
\usetikzlibrary{shapes.geometric}
\usetikzlibrary{shapes.arrows}
\usepackage{float}
\usepackage{soul}
\usepackage{amsmath}
\usepackage{float}
\usepackage{hyperref}
\usepackage{colortbl}

\usetikzlibrary{positioning,shapes,arrows}

\usepackage{graphicx,adjustbox}

\usepackage[british,UKenglish,USenglish,english,american]{babel}

\newcolumntype{M}[1]{D{.}{.}{1.#1}}

\graphicspath{ {images/} }

\title{A Reinforcement Learning Approach Towards an Optimal Mastermind Strategy}
\author{Will Fischer \& Nabil Cheikh\\}

\begin{document}
  \maketitle 

\renewcommand{\abstractname}{\vspace{-\baselineskip}} % Seriously WTF LaTeX

\begin{abstract}

A reinforcement learning algorithm was developed to play the logic puzzle Mastermind. It was less effective at solving the puzzles in few guesses than other previously researched methods, but achieved better results than other methods with little hard-coded knowledge of the problem. It was also significantly faster, and scaled better with increasing problem size than other methods.

\end{abstract}

\section{Introduction}\label{sec:problem-1}

Mastermind is a logic puzzle where the player tries to guess a hidden sequence of items, in the usual case these are pegs of various colors. The feedback that the player receives after each guess is how many of the entries in their guess are the correct color in the correct position ($B_i$), and additionally how many times they have the correct color in the wrong position ($W_i$). The game is traditionally played with the number of pegs $P = 4$ and the number of colors $N = 6$, but it can be played with any parameters $P \geq 1$  and $N \geq 2$. Figure 1 below is an example of an on going game of Mastermind. The black number, $B_i$, represents the correct number of colors in the correct position, or "black pegs". The white white number, $W_i$, is the number of correct colors in the wrong position in a guess, or "white pegs". In the Figure 1 game we have N equal to 6, and P equal to 4, denoted as (6,4).

\begin{figure}[H]
	\centering
	 \begin{adjustbox}{max width=1.0\textwidth}
		\includegraphics{fig_1}
	\end{adjustbox}
	
	\caption{Sample Mastermind Game (6,4)}
\end{figure}

In the example game above , after the first guess we see that $B_i = 0$, or there are no "black pegs". We also see that $W_i = 2$, or there are two  "white pegs", which means that none of the guessed color pegs are in the right position, however two are the right color in the wrong position in the sequence.  After the third guess we see two $B_i = 2$ and $W_i = 2$, which correspond to there being two colored pegs in the right position and of the right color and two color pegs of the right color in the wrong position.

The game traditionally uses 4 pegs and 6 possible colors, and players usually have a limited number guesses to try and guess the correct sequence. Thus the  strategies for Mastermind seek to minimize the number of guesses required to devise the correct color sequence. In the past two distinct approaches have been applied, enumeration based algorithms and a genetic algorithm. One of the earliest approaches using enumeration was developed by Donald Knuth\cite{knuth}, based on a pre-computed lookup table for what to guess. By following the instructions, it is possible to always solve the (6,4) variant within 5 guesses. However, constructing this table requires enumerating the entire state space, which is $N^P$. In a traditional game the state space is $6^4$, or 1296, and as the search space increases in size, enumeration becomes impractical. Other, more effective approaches based on reducing the entropy of the possible outcomes have been studied\cite{entropy}, but this also requires work that scales with the number of possible strings.

We sought to use reinforcement learning to try and learn an optimal strategy that could be used to solve the mastermind game in as few moves as possible, and would also scale with increased variant sizes in both P and N.  We are thus going to examine the problem more in depth as well as the previous approaches. We will then look at our implementation, its results and the conclusions we were able to draw.

\section{Problem}\label{sec:problem-2}
As the goal is to achieve a correct guess in a limited number of guesses, a strategy should seek to minimize the number of guesses required to achieve a correct sequence. As stated, a sequence will consist of P items, in our case pegs, and thus a player's guess will consist of P pins to place from N number of items, or colors.  We used the traditional model where N represents the number of colors, and P is the length of the sequence. Thus a player could play any combination of N to achieve the required number of pins P for each guess, denoted as (N,P).  Any arbitrary game space has size $N^P$, and thus as the original game had 6 Colors and 4 Pins to place, (6,4), the search space is $6^4$ or 1296.

Furthermore a strategy for Mastermind can utilize the concept of consistency, which involves using the previous information learned from previous guesses to make the next guess. Thus if one is guessing, and ignoring the given information after each guess, one would be inconsistent.  

Our goal was to use a reinforcement learning agent to learn a strategy that would require as few guesses as possible to ascertain the correct sequence. We wanted to compare a reinforced learning agent to the results of past algorithms based on enumeration and genetic methods, and also against simpler, less intelligent algorithms.

\section{Previous Approaches}\label{sec:problem-3}
There are two major categories to the existing approaches, those that employ enumeration and a genetic algorithm based approach. We proceed to examine these two approaches so as to better illustrate the existing work and mechanisms to solve this problem.
\subsection{Enumeration}\label{sec:problem-3.1}
Knuth\cite{knuth} was one of the first to employ an enumeration based search strategy, and he used it to solve the traditional game comprised of 6 Colors and 4 Pins for each guess, denoted (6,4). The subsequent strategies also employed enumeration of the search space, with different algorithms employed to optimally search this space. The table below in Figure 2 shows the average and max number of guesses for some of full enumeration algorithms as measured in\cite{ga} for a (6,4) game. 

\begin{figure}[H]
\begin{adjustbox}{center}
	\begin{tabular}{ | l | l | l | }
    \hline
    Algorithm & Average & Maximum number of guesses \\ \hline
    Knuth & 4.478 & 5 \\ \hline
    Norvig & 4.47 & 6 \\ \hline
	Kooi & 4.373 & 6 \\ \hline
	Irving & 	4.369 & 6 \\ \hline
	Neuwirth & 4.3642 & 6 \\ \hline
	Koyama and Lai	& 4.34 & 5 \\ \hline
	\end{tabular}
\end{adjustbox}
\caption{Average and maximum number of guesses for 6 colors, 4 pegs}
\end{figure}

As one can see the initial Knuth algorithm is no longer the fastest, however each algorithm is very fast, as on average they all guess the sequence in under 5 guesses. While these algorithms are fast, they do have one large weakness as they require full enumeration of the search space, which is $N^P$. Thus as game sizes increase in both color and sequence length the search space size can grow rapidly.
\subsection{Genetic}\label{sec:problem-3.2}
Bergman, Goossens, and Leus present a genetic algorithm approach to achieving an optimal number of guesses for a sequence.  The genetic approach used an initial guess to then generate a population by performing various changes and mutations to the guess, then calculating the fitness value of each mutation and finally adding eligible mutations into the generation.  This follows a standard genetic algorithm methodology, and was in fact very successful. The table in Figure 3 shows the Genetic Algorithm Performance as seen in \cite{ga} for a (6,4) game alongside the previous enumeration algorithms.

\begin{figure}[H]
\begin{adjustbox}{center}
	\begin{tabular}{ | l | l | l | }
    \hline
    Algorithm & Average & Maximum number of guesses \\ \hline
    Knuth & 4.478 & 5 \\ \hline
    Norvig & 4.47 & 6 \\ \hline
	Kooi & 4.373 & 6 \\ \hline
	Irving & 	4.369 & 6 \\ \hline
	Neuwirth & 4.3642 & 6 \\ \hline
	Koyama and Lai	& 4.34 & 5 \\ \hline
	\textbf{BGL GA} & \textbf{4.39} & \textbf{7} \\ \hline
	\end{tabular}
\end{adjustbox}
\caption{Algorithm Performance Table}
\end{figure}

The Bergman, Goossens, and Leus genetic algorithm approach is almost as fast as the fastest enumeration algorithm, and faster than some of the others. One crucial advantage is that the genetic algorithm's speed scales better with the size of the state space. As part of the analysis done by Bergman, Goossens, and Leus, they state "Our new algorithm is more time-consuming for low values of P and N, but the computational effort rises only slightly for larger instances, while the other algorithms are clearly less preferable for higher P and N."\cite{ga}.  Thus the Genetic Algorithm approach is far less resource intensive as N and P are increased.
\section{Reinforcement Learning}\label{sec:problem-4}
We sought to use a reinforcement learning agent to learn an optimal strategy that can be used to win a game with the least number of guesses.  We sought an RL solution as it should also avoid the same problem with enumeration that the Genetic Algorithm did, in that the size of N and P will affect the run time with a lesser impact than in enumeration. We specifically opted to use a Q-Learning approach as it is a model-free approach, and we thus hoped to minimize resources required by the agent. Q-Learning, as with other RL approaches, requires the use of rewards, state and transition space and differs in that it uses an action utility instead of a learning utility.  As a result we also needed to develop actions that could be taken and learned from to satisfy the Q-Learning parameters.

\section{Implementation}\label{sec:problem-5}

In our implementation of a Q-Learner we sought to reduce the sizes of the state and transition spaces and improve learning speed. We thus chose to have the agent base its decisions off of only its best guess's $B_i$ and $W_i$ (the numbers of black and white pegs), and have its actions correspond to modifying the best known guess through some number of random mutations and exchanges. If the resulting guess was worse than the one it was created from, the agent would go back to the previous one and try again.

The reward function was designed to encourage the agent to reach the goal in as few moves as possible, but also to lead it towards guesses that are closer to the correct answer. The reward function used is $W_{black} * n_{black} + W_{white} * n_{white} - W_{black} * n_{pegs}$, where $W_{black} > W_{white}$ to make it try to reach the goal of making $n_{black} = n_{pegs}$. This reward function is always negative, leading the agent to try to finish the game in order to stop receiving negative rewards. It was determined that the best values to use are $W_{black} = 1.05, W_{white} = 1$.

The agent's exploration strategy is to choose a random action a fraction of the time equal to the learning rate, and to choose the best apparent action every other time. The q-values begin at zero, when their true value is negative due to the negative reward function, which also encourages the agent to choose each action several times to learn something about its true value. When evaluating the performance of the agent, the agent takes no random actions, only performing what it has determined to be the best available action.

\section{Results}\label{sec:problem-6}

The four algorithms being compared are our q-learning agent (described above), a na\"{i}ve random guesser (randomly guesses until it gets the right answer), a more intelligent consistent random guesser (randomly guesses options that are consistent with previous guesses), and the genetic algorithm described in Berghman, Goossens, and Leus's paper. The genetic algorithm was not available for speed testing, but the authors provided their own benchmarks relative to the intelligent guesser, which can provide a rough estimate for its speed relative to the other algorithms as well.

\begin{figure}[H]
\begin{adjustbox}{center}
    \begin{tabular}{ | l | l | l | }
    \hline
    Algorithm               & Avg. Guesses   & Speed per game\\ \hline
    Q learner               & 26.6           & $9.79\ \mu s$ \\ \hline
    Na\"{i}ve Random Guesser       & 1300           & $120\ \mu s$  \\ \hline
    Consistent Random Guesser      & 4.64           & $238\ \mu s$  \\ \hline
    BGL GA                  & 4.39           & $146\ ms$ (extrapolated) \\ \hline
\end{tabular}
\end{adjustbox}
\caption{Average required guesses and speed for tested methods (6 colors, 4 pegs). The Q learning agent was trained for 5 million games before testing with $\alpha = 0.01$.}
\end{figure}

The Q learner has a massive speed advantage over all other implementations\footnote{All algorithms tested on an i5-4670K @ 4.5 GHz}, but takes many more guesses than any but the most na\"{i}ve random alternative. Our agent takes approximately the same amount of time per iteration when learning as when being tested, so the up-front training cost is not a significant burden if the number of times the agent is used is similar to the number of times it is trained.

\begin{figure}[H]
\begin{adjustbox}{center}
    \begin{tabular}{ | l | l | l | }
    \hline
    Algorithm               & Avg. Guesses      & Speed per game\\ \hline
    Q learner               & 47.0              & $47.0\ \mu s$ \\ \hline
    Na\"{i}ve Random Guesser       & 520,000           & $68.9\ ms$    \\ \hline
    Consistent Random Guesser      & 6.71              & $145\ ms$     \\ \hline
    BGL GA                  & 6.475             & $145\ ms$ (extrapolated) \\ \hline
\end{tabular}
\end{adjustbox}
\caption{Average required guesses and speed for tested methods (9 colors, 6 pegs). The Q learning agent was trained for 1 million games before testing with $\alpha = 0.1$.}
\end{figure}

Our Q learning agent continues to have a large speed advantage over other implementations. 

\begin{figure}[H]
\begin{adjustbox}{center}
    \begin{tabular}{ | l | l | l | }
    \hline
    Algorithm               & Avg. Guesses      & Speed per game\\ \hline
    Q learner               & 207               & $133\ \mu s$  \\ \hline
    Na\"{i}ve Random Guesser       & 550,000,000       & $93.3\ s$     \\ \hline
    Consistent Random  Guesser      & 8.57              & $14.5\ s$     \\ \hline
    BGL GA                  & 8.366             & $193\ ms$ (extrapolated) \\ \hline
\end{tabular}
\end{adjustbox}
\caption{Average required guesses and speed for tested methods (12 colors, 8 pegs). The Q learning agent was trained for 1 million games before testing with $\alpha = 0.1$.}
\end{figure}

\section{Conclusion}\label{sec:problem-7}

Our reinforcement learning agent is not competitive with other agents for this problem, as the goal is to solve the sequence in as few guesses as possible. It solves the sequence quite a bit faster, but it also requires many more guesses to guess the correct sequence.  
While we were not able to achieve a comparable number of guesses with the faster algorithms, we were able to achieve an advantage over the enumeration methods in that we are less constrained by the size of the game.  The reinforcement learner thus scales in relation to larger values of N and P far better than enumeration approaches.

Thus this is still a promising result given the simplicity of the agent's knowledge of the problem. It has no built-in conception of the logical meaning of the feedback it receives from the game, only that it's better to have more black and white pegs, and that black pegs are better than white pegs. It doesn't perform any inference based on the rules of the game, but it is still able to come up with reasonable solutions extremely quickly. This shows the general applicability of reinforcement learning to systems that are not well understood.

\begin{thebibliography}{9}

%\bibitem{nphard} "Mastermind is NP-Complete" - Jeff Stuckman, Guo-Qiang Zhang - \url{arxiv.org/abs/cs/0512049}

\bibitem{knuth} "The Computer as Master Mind" - Donald E. Knuth -
\url{http://www.dcc.fc.up.pt/~sssousa/RM09101.pdf}

\bibitem{ga} "Efficient solutions for Mastermind using genetic algorithms" - Lotte Berghman, Dries Goossens, Roel Leus  - \url{https://lirias.kuleuven.be/bitstream/123456789/164803/1/kbi_0806.pdf}

\bibitem{entropy} "Yet Another Mastermind Strategy" - Barteld Kooi - \url{http://www.researchgate.net/profile/Barteld_Kooi/publication/30485793_Yet_another_mastermind_strategy/links/0c96051c9a68c6f6bd000000.pdf}

\end{thebibliography}

\end{document}