use guess;
pub mod random;
pub mod random_consistent;
pub mod qlearner;
pub mod random_no_repeat;

pub trait Agent {    
    // Returns the number of guesses required to correctly determine the answer,
    // and also performs any learning that it needs to do
    fn solve(&mut self, answer: &guess::Answer) -> u32 {
        self.test(answer)
    }
    
    fn solve_random(&mut self, settings: guess::Settings) -> u32 {
        self.solve(&guess::Answer::random(settings))
    }
    
    // Same as solve, but doesn't perform any learning actions.
    fn test(&self, answer: &guess::Answer) -> u32;
    fn test_random(&self, settings: guess::Settings) -> u32 {
        self.test(&guess::Answer::random(settings))
    }
}

fn is_consistent(answer: &guess::Answer, 
                 history: &Vec<(guess::Answer, guess::Info)>) -> bool {
    history.iter().all(|&(ref ans, ref info)| {
        guess::check_guess(ans, answer) == *info
    })
}
