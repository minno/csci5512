use guess;
use agents::Agent;

pub struct RandomNoRepeat;

impl Agent for RandomNoRepeat {
    fn test(&self, answer: &guess::Answer) -> u32 {
        let mut guesses = Vec::new();
        loop {
            let mut new_state = guess::Answer::random(answer.settings);
            while guesses.iter().any(|guess| *guess == new_state) {
                new_state = guess::Answer::random(answer.settings);
            }
            let info = guess::check_guess(&new_state, answer);
            guesses.push(new_state);
            if info.num_black == answer.settings.num_pegs {
                return guesses.len() as u32;
            }
        }
    }
}